package fp.ch5

/**
  * Created by Ivan_Babanin on 12/21/2016.
  */
object Chapter5 {

  // Exercise 5.1
  def toList[A](s: Stream[A]): List[A] = s match {
    case Empty => Nil
    case Cons(head, tail) => head() :: toList(tail())
  }

  // Exercise 5.2
  def take[A](s: Stream[A], n: Int): List[A] = (s, n) match {
    case (Empty, _) | (_, 0) => Nil
    case (Cons(h, t), nLeft) => h() :: take(t(), nLeft - 1)
  }

  def drop[A](s: Stream[A], n: Int): List[A] = (s, n) match {
    case (Empty, _) => Nil
    case (Cons(h, t), skip) if skip <= 0 => h() :: drop(t(), 0)
    case (Cons(h, t), skip) => drop(t(), skip - 1)
  }

  // Exercise 5.3
  def takeWhile[A](as: Stream[A])(f: A => Boolean): Stream[A] = as match {
    case Empty => Empty
    case Cons(h, t) => if (f(h())) Cons(h, () => takeWhile(t())(f)) else Empty
  }

  def foldRight[A, B](as: Stream[A], zero: B)(f: (A, => B) => B): B = as match {
    case Cons(h, t) => f(h(), foldRight(t(), zero)(f))
    case Empty => zero
  }

  // Exercise 5.4
  def forAll[A](as: Stream[A])(p: A => Boolean): Boolean = as match {
    case Empty => true
    case Cons(h, t) => if (p(h())) forAll(t())(p) else false
  }

  // Exercise 5.5
  def takeWhileViaFold[A](as: Stream[A])(f: A => Boolean): Stream[A] =
    foldRight(as, Empty: Stream[A])((a, acc) => if (f(a)) Cons(() => a, () => acc) else Empty)

  // Exercise 5.6
  def headOption[A](as: Stream[A]): Option[A] = foldRight(as, None: Option[A])((a, _) => Some(a))

  // Exercise 5.7
  def map[A, B](as: Stream[A])(f: A => B): Stream[B] = foldRight(as, Empty: Stream[B])((a, acc) => Cons(() => f(a), () => acc))

  def filter[A](as: Stream[A])(f: A => Boolean): Stream[A] = foldRight(as, Empty: Stream[A])((a, acc) => if (f(a)) Cons(() => a, () => acc) else acc)

  def append[A](as: Stream[A], bs: Stream[A]): Stream[A] = foldRight(as, bs)((a, acc) => Cons(() => a, () => acc))

  def flatMap[A, B](as: Stream[A])(f: A => Stream[B]): Stream[B] = foldRight(as, Empty: Stream[B])((a, acc) => append(f(a), acc))

  // Exercise 5.8
  def ones: Stream[Int] = Cons(() => 1, () => ones)

  // Exercise 5.9
  def from(n: Int): Stream[Int] = Cons(() => n, () => from(n + 1))

  // Exercise 5.10
  def fib: Stream[Int] = {
    def loop(a: Int, b: Int): Stream[Int] = Cons(() => a + b, () => loop(b, a + b))

    Cons(() => 0, () => Cons(() => 1, () => loop(0, 1)))
  }

  // Exercise 5.11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case None => Empty
    case Some((a, s)) => Cons(() => a, () => unfold(s)(f))
  }

  // Exercise 5.12
  def fibsUnfold: Stream[Int] = unfold((0, 1)) { case (cur, next) => Some(cur, (next, cur + next)) }

  def fromUnfold(n: Int): Stream[Int] = unfold(n)(n => Some(n, n + 1))

  def constantUnfold[A](value: A): Stream[A] = unfold(None)(_ => Some(value, None))

  def onesUnfold: Stream[Int] = unfold(None)(_ => Some(1, None))

  // Exercise 5.13
  def mapUnfold[A, B](as: Stream[A])(f: A => B): Stream[B] = unfold(as) {
    case Cons(h, t) => Some((f(h()), t()))
    case _ => None
  }

  def takeUnfold[A](as: Stream[A], n: Int): Stream[A] = unfold((n, as)) {
    case (0, _) | (_, Empty) => None
    case (rest, Cons(h, t)) => Some((h(), (rest - 1, t())))
  }

  def takeWhileUnfold[A](as: Stream[A])(p: A => Boolean): Stream[A] = unfold(as) {
    case Cons(h, t) if p(h()) => Some(h(), t())
    case _ => None
  }

  def zipWithUnfold[A, B, C](as: Stream[A], bs: Stream[B])(f: (A, B) => C): Stream[C] =
    unfold((as, bs)) {
      case (Empty, _) | (_, Empty) => None
      case (Cons(ah, at), Cons(bh, bt)) => Some((f(ah(), bh()), (at(), bt())))
    }

  def zipAllUnfold[A, B](as: Stream[A], bs: Stream[B]): Stream[(Option[A], Option[B])] =
    unfold[(Option[A], Option[B]), (Stream[A], Stream[B])]((as, bs)) {
      case (Empty, Empty) => None
      case (Empty, Cons(h, t)) => Some(((None, Some(h())), (Empty, t())))
      case (Cons(h, t), Empty) => Some((Some(h()), None), (t(), Empty))
      case (Cons(ah, at), Cons(bh, bt)) => Some((Some(ah()), Some(bh())), (at(), bt()))
    }

  // Exercise 5.14
  def startsWith[A](as: Stream[A], bs: Stream[A]): Boolean = foldRight(zipAllUnfold(as, bs), true)((o, acc) => o match {
    case (None, None) | (_, None) => acc
    case (Some(a), Some(b)) if a == b => acc
    case _ => false
  })

  // Exercise 5.15
  def tails[A](as: Stream[A]): Stream[Stream[A]] = Cons(() => as, () => unfold(as) {
    case Cons(_, t) => Some(t(), t())
    case _ => None
  })

  // Exercise 5.16
  def scanRight[A](as: Stream[A], zero: A)(f: (A, A) => A): Stream[A] = as match {
    case Empty => Stream[A](zero)
    case Cons(h, t) =>
      scanRight(t(), zero)(f) match {
        case s @ Cons(h2, _) => Cons(() => f(h(), h2()), () => s)
      }
  }
}
