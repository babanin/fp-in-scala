package fp.ch5

/**
  * Created by Ivan_Babanin on 12/21/2016.
  */
sealed trait Stream[+A]

case object Empty extends Stream[Nothing]

case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A] {
  override def equals(obj: scala.Any): Boolean =
    obj match {
      case other: Cons[A] => other.h() == h()
      case _ => false
    }

  override def toString: String = s"(${h().toString}${t().toString})"
}

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] = if (as.isEmpty) Empty else cons(as.head, apply(as.tail: _*))
}
