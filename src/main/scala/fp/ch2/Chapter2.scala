package fp.ch2

import scala.annotation.tailrec

object Chapter2 {
  // Exercise 2.1
  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case 2 => 1
    case n => fib(n - 1) + fib(n - 2)
  }

  // Exercise 2.2
  def isSorted[A](as: Array[A])(ordered: (A, A) => Boolean): Boolean = {
    @tailrec
    def loop(n: Int): Boolean = {
      if (n >= as.length) {
        true
      } else if (!ordered(as(n - 1), as(n))) {
        false
      } else {
        loop(n + 1)
      }
    }

    loop(1)
  }

  // Exercise 2.3
  def curry[A, B, C](f: (A, B) => C): A => (B => C) = (a: A) => (b: B) => f(a, b)

  // Exercise 2.4
  def uncurry[A, B, C](f: (A => B => C)): (A, B) => C = (a: A, b: B) => f(a)(b)

  // Exercise 2.5
  def compose[A, B, C](f: B => C, g: A => B) : A => C = (a : A) => f(g(a))

}
