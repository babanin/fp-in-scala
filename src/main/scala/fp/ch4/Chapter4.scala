package fp.ch4

/**
  * Created by Ivan_Babanin on 12/19/2016.
  */
object Chapter4 {
  // Exercise 4.2
  def variance(xs: Seq[Double]): Option[Double] = {
    mean(xs).flatMap(m => if (xs.isEmpty) None else Some(xs.map(x => math.pow(m - x, 2)).sum / xs.length))
  }

  def mean(xs: Seq[Double]): Option[Double] = {
    if (xs.isEmpty) None else Some(xs.sum / xs.length)
  }

  // Exercise 4.3
  def map2[A, B, C](oa: Option[A], ob: Option[B])(f: (A, B) => C): Option[C] = (oa, ob) match {
    case (None, _) | (_, None) => None
    case (Some(a), Some(b)) => Some(f(a, b))
  }

  // Exercise 4.4
  def sequence[A](a: List[Option[A]]): Option[List[A]] = a match {
    case Some(x) :: xs => sequence(xs).flatMap(xx => Some(x :: xx))
    case Nil => Some(Nil)
    case None :: _ => None
  }

  // Exercise 4.5
  def traverse[A, B](as: List[A])(f: A => Option[B]): Option[List[B]] = as match {
    case Nil => Some(Nil)
    case x :: xs => for {v <- f(x); vv <- traverse(xs)(f)} yield v :: vv
  }

  // Exercise 4.7
  def sequenceEither[E, A](as: List[Either[E, A]]): Either[E, List[A]] = as match {
    case Nil => Right(Nil)
    case x :: xs => for {v <- x; vv <- sequenceEither(xs)} yield v :: vv
  }

  def traverseEither[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = as match {
    case Nil => Right(Nil)
    case x :: xs => for {v <- f(x); vv <- traverseEither(xs)(f)} yield v :: vv
  }
}
