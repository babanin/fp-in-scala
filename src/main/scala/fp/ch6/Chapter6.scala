package fp.ch6

/**
  * @author Ivan Babanin (babanin@gmail.com)
  */
object Chapter6 {
  // Exercise 6.1
  def nonNegativeInt(rng: RNG): (Int, RNG) = {
    val (v, nRng) = rng.nextInt
    (if (v < 0) -(v + 1) else v, nRng)
  }

  // Exercise 6.2
  def double(rng: RNG): (Double, RNG) = {
    val (i, r) = rng.nextInt
    (i.toDouble / Int.MaxValue, rng)
  }

  // Exercise 6.3
  def intDouble(rng: RNG): ((Int, Double), RNG) = {
    val (i, iRng) = rng.nextInt
    val (d, dRng) = double(iRng)
    ((i, d), dRng)
  }

  def doubleInt(rng: RNG): ((Double, Int), RNG) = intDouble(rng) match {
    case ((i, d), r) => ((d, i), r)
  }

  def double3(d1Rng: RNG): ((Double, Double, Double), RNG) = {
    val (d1, d2Rng) = double(d1Rng)
    val (d2, d3Rng) = double(d2Rng)
    val (d3, rng) = double(d3Rng)

    ((d1, d2, d3), rng)
  }

  // Exercise 6.4
  def ints(count: Int)(rng: RNG): (List[Int], RNG) =
    if (count == 1) {
      val (i, r) = rng.nextInt
      (List(i), r)
    } else {
      val (i, r) = rng.nextInt
      val (l, r2) = ints(count - 1)(r)
      (i :: l, r2)
    }

  type Rand[+A] = RNG => (A, RNG)

  def int: Rand[Int] = _.nextInt

  def unit[A](a: A): Rand[A] = (a, _)

  def map[A, B](s: Rand[A])(f: A => B): Rand[B] = rng => {
    val (a, rng2) = s(rng)
    (f(a), rng2)
  }

  def nonNegativeInt: Rand[Int] = map(int)(i => if (i < 0) -(i + 1) else i)

  def nonNegativeEven: Rand[Int] = map(nonNegativeInt)(i => i - i % 2)

  // Exercise 6.5
  def doubleMap: Rand[Double] = map(rng => rng.nextInt)(_ * 1.0 / Int.MaxValue)

  // Exercise 6.6
  def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = { rng =>
    val (a, rng2) = ra(rng)
    val (b, rng3) = rb(rng2)
    (f(a, b), rng3)
  }

  def both[A, B](ra: Rand[A], rb: Rand[B]): Rand[(A, B)] = map2(ra, rb)((_, _))

  def randIntDouble: Rand[(Int, Double)] = both(int, double)

  // Exercise 6.7
  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] = { rng =>
    fs match {
      case List() => (List(), rng)
      case x :: xs =>
        val (v, vr) = x(rng)
        val (n, nr): (List[A], RNG) = sequence(xs)(vr)
        (v :: n, nr)
    }
  }

  def sequenceFoldRight[A](fs: List[Rand[A]]): Rand[List[A]] = fs.foldRight(unit(List[A]()))((f, acc) => map2(f, acc)(_ :: _))

  // Exercise 6.8
  def flatMap[A, B](f: Rand[A])(g: A => Rand[B]): Rand[B] = { rng =>
    val (a, rng2) = f(rng)
    g(a)(rng2)
  }

  // Exercise 6.9
  def mapFlat[A, B](ra: Rand[A])(f: A => B): Rand[B] = flatMap(ra)(a => unit(f(a)))

  def map2Flat[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = flatMap(ra)(a => flatMap(rb)(b => unit(f(a, b))))

  // Exercise 6.10 in State class
}
