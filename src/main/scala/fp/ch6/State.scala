package fp.ch6

import fp.ch6.State.unit

/**
  * @author Ivan Babanin (babanin@gmail.com)
  */
case class State[S, +A](run: S => (A, S)) {
  def map[B](f: A => B): State[S, B] = State(s => {
    val (a, s2) = run(s)
    (f(a), s2)
  })

  def map2[B, C](sb: State[S, B])(f: (A, B) => C): State[S, C] = State(
    s => {
      val (a, s2) = run(s)
      val (b, s3) = sb.run(s2)
      (f(a, b), s3)
    }
  )

  def flatMap[B](f: A => State[S, B]): State[S, B] = State(s => {
    val (a, s2) = run(s)
    f(a).run(s2)
  })

  //  def sequence[T >: S](ls: List[State[T, A]]): State[T, List[A]] = ls.foldRight(unit[T, List[A]](List[A]()))((s, acc) => s.map2(acc)(_ :: _))
}

object State {
  def unit[S, A](a: A): State[S, A] = State(s => (a, s))

  def sequence[S, A](ls: List[State[S, A]]): State[S, List[A]] = ls.foldRight(unit[S, List[A]](List[A]()))((s, acc) => s.map2(acc)(_ :: _))

  def get[S]: State[S, S] = State(s => (s, s))

  def set[S](s: S): State[S, Unit] = State(_ => ((), s))
}
