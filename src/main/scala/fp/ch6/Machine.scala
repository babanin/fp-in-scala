package fp.ch6

/**
  * Created by Ivan_Babanin on 1/16/2017.
  */
sealed trait Input

case object Coin extends Input

case object Turn extends Input

case class Machine(locked: Boolean, candies: Int, coins: Int)


object Machine extends App {
  type MachineState = State[Machine, (Int, Int)]

    def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = inputs.flatMap()

  //  def handle(input: Input, machine: Machine): ((Int, Int), Machine) = input match {
  //
  //  }

  //  private val tuple: ((Int, Int), Machine) = simulateMachine(List(Coin, Coin, Coin, Turn)).run(Machine(locked = false, 5, 10))

  //  println(s"Candies: ${tuple._1._1} Coins: ${tuple._1._2}")

  val m1 = State { s: String => (s.length, s) }

  def repeat(num: Int): State[String, Unit] = State[String, Unit] { s: String => ((), s * num) }

  println(m1.flatMap(repeat).flatMap({ _ => m1 }).run("hello"))
}