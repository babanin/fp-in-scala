package fp.ch3

import scala.annotation.tailrec

/**
  * @author Ivan Babanin (babanin@gmail.com)
  */
object Chapter3 {
  // Exercise 3.2
  def tail[A](as: List[A]): List[A] = as match {
    case Nil => throw new IllegalArgumentException("Empty list")
    case Cons(x, xs) => xs
  }

  // Exercise 3.3
  def setHead[A](as: List[A], a: A): List[A] = as match {
    case Nil => throw new IllegalArgumentException("Empty list")
    case Cons(x, xs) => Cons(a, xs)
  }

  // Exercise 3.4
  def drop[A](as: List[A], n: Int): List[A] = as match {
    case Nil => Nil
    case Cons(x, xs) if n > 0 => drop(xs, n - 1)
    case _ => as
  }

  // Exercise 3.5
  def dropWhile[A](as: List[A], f: A => Boolean): List[A] = as match {
    case Nil => Nil
    case Cons(x, xs) if f(x) => dropWhile(xs, f)
    case _ => as
  }

  // Exercise 3.6
  def init[A](as: List[A]): List[A] = as match {
    case Cons(x, Cons(y, Nil)) => Cons(x, Nil)
    case Cons(x, Nil) => Nil
    case Cons(x, xs) => Cons(x, init(xs))
    case Nil => Nil
  }

  def foldRight[A, B](as: List[A], zero: B, f: (A, B) => B): B = as match {
    case Nil => zero
    case Cons(x, xs) => f(x, foldRight(xs, zero, f))
  }

  def foldRightC[A, B](as: List[A], zero: B)(f: (A, B) => B): B = foldRight(as, zero, f)

  def product(as: List[Double]): Double = {
    foldRight(as, 1.0, (a: Double, acc: Double) => acc * a)
  }

  // Exercise 3.9
  def length[A](as: List[A]): Int = foldRightC(as, 0)((_, acc) => acc + 1)

  // Exercise 3.10
  @tailrec
  def foldLeft[A, B](as: List[A], acc: B)(f: (B, A) => B): B = as match {
    case Nil => acc
    case Cons(x, xs) => foldLeft(xs, f(acc, x))(f)
  }

  // Exercise 3.11
  def sumLeft(as: List[Int]): Int = foldLeft(as, 0)(_ + _)

  def productLeft(as: List[Double]): Double = foldLeft(as, 1.0)(_ * _)

  // Exercise 3.12
  def reverse[A](as: List[A]): List[A] = foldLeft(as, Nil: List[A])((acc: List[A], a: A) => Cons(a, acc))

  // Exercise 3.13
  def foldRightByFoldLeft[A, B](as: List[A], zero: B)(f: (A, B) => B): B = foldLeft(reverse(as), zero)((a, b) => f(b, a))

  def foldLefByFoldRight[A, B](as: List[A], zero: B)(f: (A, B) => B): B = ???

  // Exercise 3.14
  def append[A](as: List[A], bs: List[A]): List[A] = foldRight[A, List[A]](as, bs, (a: A, acc: List[A]) => Cons(a, acc))

  // Exercise 3.15
  def flatten[A](as: List[List[A]]): List[A] = foldRight(as, Nil, (a: List[A], acc: List[A]) => append(a, acc))

  // Exercise 3.16
  def inc(as: List[Int]): List[Int] = as match {
    case Nil => Nil
    case Cons(x, xs) => Cons(x + 1, inc(xs))
  }

  // Exercise 3.17
  def doubleToString(as: List[Double]): List[String] = as match {
    case Nil => Nil
    case Cons(x, xs) => Cons(x.toString, doubleToString(xs))
  }

  // Exercise 3.18
  def map[A, B](as: List[A])(f: A => B): List[B] = as match {
    case Nil => Nil
    case Cons(x, xs) => Cons(f(x), map(xs)(f))
  }

  // Exercise 3.19
  def filter[A](as: List[A])(p: A => Boolean): List[A] = as match {
    case Nil => Nil
    case Cons(x, xs) => if (p(x)) Cons(x, filter(xs)(p)) else filter(xs)(p)
  }

  // Exercise 3.20
  def flatMap[A, B](as: List[A])(f: A => List[B]): List[B] = as match {
    case Nil => Nil
    case Cons(x, xs) => append(f(x), flatMap(xs)(f))
  }

  // Exercise 3.21
  def filterFlatMap[A](as: List[A])(p: A => Boolean): List[A] =
    flatMap(as)(x => if (p(x)) List(x) else List())

  // Exercise 3.22
  def sumLists(as: List[Int], bs: List[Int]): List[Int] = (as, bs) match {
    case (_, Nil) => Nil
    case (Nil, _) => Nil
    case (Cons(x, xs), Cons(y, ys)) => Cons(x + y, sumLists(xs, ys))
  }

  // Exercise 3.23
  def zipWith[A, B](as: List[A], bs: List[A])(f: (A, A) => B): List[B] = (as, bs) match {
    case (_, Nil) | (Nil, _) => Nil
    case (Cons(x, xs), Cons(y, ys)) => Cons(f(x, y), zipWith(xs, ys)(f))
  }

  // Exercise 3.24
  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = {
    def loop(lsup: List[A], lsub: List[A]): Boolean = (lsup, lsub) match {
      case (Nil, Nil) | (_, Nil) => true
      case (Nil, _) => false
      case (Cons(x, xs), Cons(y, ys)) if x == y => hasSubsequence(xs, ys) || hasSubsequence(xs, sub)
      case (Cons(_, xs), _) => hasSubsequence(xs, sub)
    }

    loop(sup, sub)
  }

  // Exercise 3.25
  def count[A](tree: Tree[A]): Int = tree match {
    case Leaf(_) => 1
    case Branch(left, right) => count(left) + count(right) + 1
  }

  // Exercise 3.26
  def maximum(tree: Tree[Int]): Int = tree match {
    case Leaf(x) => x
    case Branch(left, right) => Math.max(maximum(left), maximum(right))
  }

  // Exercise 3.27
  def depth[A](tree: Tree[A]): Int = tree match {
    case Leaf(x) => 1
    case Branch(left, right) => Math.max(depth(left), depth(right)) + 1
  }

  // Exercise 3.28
  def map[A, B](tree: Tree[A])(f: A => B): Tree[B] = tree match {
    case Leaf(x) => Leaf(f(x))
    case Branch(left, right) => Branch(map(left)(f), map(right)(f))
  }

  // Exercise 3.29
  def fold[A, B](tree: Tree[A])(f: A => B)(g: (B, B) => B): B = tree match {
    case Leaf(x) => f(x)
    case Branch(left, right) => g(fold(left)(f)(g), fold(right)(f)(g))
  }

  def countViaFold[A](tree: Tree[A]): Int = fold(tree)(_ => 1)(_ + _ + 1)

  def maximumViaFold(tree: Tree[Int]): Int = fold(tree)(x => x)(Math.max)

  def depthViaFold[A](tree: Tree[A]): Int = fold(tree)(_ => 1)((x, y) => Math.max(x, y) + 1)

  def mapViaFold[A, B](tree: Tree[A])(f: A => B): Tree[B] = fold(tree)(x => Leaf(f(x)): Tree[B])(Branch(_, _))
}
