package fp.ch3

/**
  * @author Ivan Babanin (babanin@gmail.com)
  */
sealed trait Tree[+A]

case class Leaf[A](value: A) extends Tree[A]

case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
