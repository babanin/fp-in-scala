package fp

import org.scalatest.FunSuite

/**
  * Created by Ivan_Babanin on 12/21/2016.
  */
class Chapter5Suite extends FunSuite {

  import fp.ch5._

  test("Exercise 5.1") {
    import fp.ch5.Chapter5.toList

    assert(List(1, 2, 3) == toList(Stream(1, 2, 3)))
  }

  test("Exercise 5.2") {
    import fp.ch5.Chapter5.{take, drop}

    assert(List(1, 2, 3, 4) == take(Stream(1, 2, 3, 4, 5), 4))
    assert(List(1, 2, 3, 4) == drop(Stream(-1, 0, 1, 2, 3, 4), 2))
  }

  test("Exercise 5.3") {
    import fp.ch5.Chapter5.{toList, takeWhile}

    assert(List(1, 2, 3, 4) == toList(takeWhile(Stream(1, 2, 3, 4, 5, 6))(_ < 5)))
  }

  test("Exercise 5.4") {
    import fp.ch5.Chapter5.forAll

    assert(forAll(Stream(1, 2, 3, 4, 5))(_ > 0))
    assert(!forAll(Stream(1, 2, 3, 4, 5))(_ < 0))
  }

  test("Exercise 5.5") {
    import fp.ch5.Chapter5.{takeWhileViaFold, toList}

    assert(List(1, 2, 3) == toList(takeWhileViaFold(Stream(1, 2, 3, 4, 5))(_ < 4)))
  }

  test("Exercise 5.6") {
    import fp.ch5.Chapter5._

    assert(headOption(Stream(5)).contains(5))
    assert(headOption(Stream()).isEmpty)
  }

  test("Exercise 5.7") {
    import fp.ch5.Chapter5._

    assert(List(1, 2, 3, 4, 5) == toList(map(Stream(0, 1, 2, 3, 4))(_ + 1)))
    assert(List(1, 2, 3, 4, 5) == toList(filter(Stream(0, 1, 2, 3, 4, 5))(_ > 0)))
    assert(List(1, 2, 3, 4, 5) == toList(append(Stream(1, 2, 3), Stream(4, 5))))
    assert(List(1, 1, 2, 2, 3, 3) == toList(flatMap(Stream(1, 2, 3))(x => Stream(x, x))))
  }

  test("Exercise 5.8") {
    import fp.ch5.Chapter5._

    assert(List(1, 1, 1, 1, 1) == take(ones, 5))
  }

  test("Exercise 5.9") {
    import fp.ch5.Chapter5.{from, take}

    assert(List(1, 2, 3, 4, 5) == take(from(1), 5))
  }

  test("Exercise 5.10") {
    import fp.ch5.Chapter5.{fib, take}

    assert(List(0, 1, 1, 2, 3, 5) == take(fib, 6))
  }

  test("Exercise 5.11") {
    import fp.ch5.Chapter5.{unfold}
  }

  test("Exercise 5.12") {
    import fp.ch5.Chapter5.{take, fibsUnfold, constantUnfold, onesUnfold, fromUnfold}

    assert(List(0, 1, 1, 2, 3, 5) == take(fibsUnfold, 6))
    assert(List(2, 2, 2, 2, 2) == take(constantUnfold(2), 5))
    assert(List(1, 1, 1, 1, 1) == take(onesUnfold, 5))
    assert(List(1, 2, 3, 4, 5) == take(fromUnfold(1), 5))
  }

  test("Exercise 5.13") {
    import fp.ch5.Chapter5.{toList, mapUnfold, takeUnfold, takeWhileUnfold, zipAllUnfold, zipWithUnfold}

    assert(List(1, 2, 3, 4) == toList(mapUnfold(Stream(0, 1, 2, 3))(_ + 1)))
    assert(List(1, 2) == toList(takeUnfold(Stream(1, 2, 3, 4, 5, 6), 2)))
    assert(List(1, 2) == toList(takeWhileUnfold(Stream(1, 2, 3))(_ < 3)))
    assert(List(2, 4, 6, 8) == toList(zipWithUnfold(Stream(1, 2, 3, 4), Stream(1, 2, 3, 4))(_ + _)))
    assert(List((Some(1), Some(1)), (Some(2), None)) == toList(zipAllUnfold(Stream(1, 2), Stream(1))))
  }

  test("Exercise 5.14") {
    import fp.ch5.Chapter5.startsWith

    assert(startsWith(Stream(), Stream()))
    assert(startsWith(Stream(1, 2, 3), Stream()))
    assert(startsWith(Stream(1, 2, 3), Stream(1)))
    assert(startsWith(Stream(1, 2, 3), Stream(1, 2)))
    assert(startsWith(Stream(1, 2, 3), Stream(1, 2, 3)))
    assert(!startsWith(Stream(1, 2, 3), Stream(1, 2, 3, 4)))
  }

  test("Exercise 5.15") {
    import fp.ch5.Chapter5.tails

    assert(Stream(Stream(1, 2, 3), Stream(2, 3), Stream(3)) == tails(Stream(1, 2, 3)))
    assert(Stream(Stream(3)) != tails(Stream(1, 2, 3)))
  }

  test("Exercise 5.16") {
    import fp.ch5.Chapter5.{scanRight, toList}

    assert(List(6, 5, 3, 0) == toList(scanRight(Stream(1, 2, 3), 0)(_ + _)))
  }
}
