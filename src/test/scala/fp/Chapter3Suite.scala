package fp

import fp.ch3.Chapter3.depth
import fp.ch3.{Branch, Leaf}
import org.scalatest.FunSuite

/**
  * @author Ivan Babanin (babanin@gmail.com)
  */
class Chapter3Suite extends FunSuite {

  import fp.ch3.List
  import fp.ch3.Nil

  test("Exercise 3.2") {
    import fp.ch3.Chapter3.tail

    assert(List(1, 2, 3) == tail(List(0, 1, 2, 3)))
  }

  test("Exercise 3.3") {
    import fp.ch3.Chapter3.setHead

    assert(List(1, 2, 3) == setHead(List(0, 2, 3), 1))
  }

  test("Exercise 3.4") {
    import fp.ch3.Chapter3.drop

    assert(Nil == drop(Nil, 2))
    assert(Nil == drop(List(1), 2))
    assert(List(2, 3, 4) == drop(List(0, 1, 2, 3, 4), 2))
  }

  test("Exercise 3.5") {
    import fp.ch3.Chapter3.dropWhile

    assert(List(1, 2) == dropWhile(List(1, 2), (x: Int) => x < 0))
    assert(Nil == dropWhile(List(1, 2, 3, 4, 5, 6), (x: Int) => x < 10))
    assert(List(5, 6) == dropWhile(List(1, 2, 3, 4, 5, 6), (x: Int) => x < 5))
  }

  test("Exercise 3.6") {
    import fp.ch3.Chapter3.init

    assert(Nil == init(Nil))
    assert(Nil == init(List(1)))
    assert(List(1, 2) == init(List(1, 2, 3)))
  }

  test("Exercise 3.9") {
    import fp.ch3.Chapter3.length

    assert(0 == length(Nil))
    assert(1 == length(List(1)))
    assert(2 == length(List(1, 2)))
  }

  test("Exercise 3.10") {
    import fp.ch3.Chapter3.foldLeft

    assert(0 == foldLeft(Nil: List[Int], 0)(_ + _))
    assert(1 == foldLeft(List(1), 0)(_ + _))
    assert(3 == foldLeft(List(1, 2), 0)(_ + _))
    assert(-5 == foldLeft(List(1, 2, 3, 4, 5), 10)(_ - _))
  }

  test("Exercise 3.11") {
    import fp.ch3.Chapter3.sumLeft
    import fp.ch3.Chapter3.productLeft

    assert(10 == sumLeft(List(1, 2, 3, 4)))
    assert(24 == productLeft(List(1, 2, 3, 4)))
  }

  test("Exercise 3.12") {
    import fp.ch3.Chapter3.reverse

    assert(Nil == reverse(Nil))
    assert(List(3, 2, 1) == reverse(List(1, 2, 3)))
  }

  test("Exercise 3.13") {
    import fp.ch3.Chapter3.foldRightByFoldLeft

    assert(3 == foldRightByFoldLeft(List(0, 1, 2), 0)(_ + _))
  }

  test("Exercise 3.14") {
    import fp.ch3.Chapter3.append

    assert(List(1, 2, 3, 4) == append(List(1, 2), List(3, 4)))
  }

  test("Exercise 3.15") {
    import fp.ch3.Chapter3.flatten

    assert(List(1, 2, 3, 4, 5, 6) == flatten(List(List(1, 2), List(3, 4), List(5, 6))))
  }

  test("Exercise 3.16") {
    import fp.ch3.Chapter3.inc

    assert(List(2, 3, 4, 5, 6) == inc(List(1, 2, 3, 4, 5)))
  }

  test("Exercise 3.17") {
    import fp.ch3.Chapter3.doubleToString

    assert(List("1.0", "2.0", "3.0") == doubleToString(List(1.0, 2.0, 3.0)))
  }

  test("Exercise 3.18") {
    import fp.ch3.Chapter3.map

    assert(List(2, 3, 4, 5, 6) == map(List(1, 2, 3, 4, 5))(_ + 1))
  }

  test("Exercise 3.19") {
    import fp.ch3.Chapter3.filter

    assert(List(1, 3, 5) == filter(List(1, 2, 3, 4, 5))(_ % 2 == 1))
  }

  test("Exercise 3.20") {
    import fp.ch3.Chapter3.flatMap

    assert(List(1, 1, 2, 2, 3, 3) == flatMap(List(1, 2, 3))(x => List(x, x)))
  }

  test("Exercise 3.21") {
    import fp.ch3.Chapter3.filterFlatMap

    assert(List(1, 3, 5) == filterFlatMap(List(1, 2, 3, 4, 5))(_ % 2 == 1))
  }

  test("Exercise 3.22") {
    import fp.ch3.Chapter3.sumLists

    assert(List(5, 7, 9) == sumLists(List(1, 2, 3), List(4, 5, 6)))
  }

  test("Exercise 3.23") {
    import fp.ch3.Chapter3.zipWith

    assert(List(5) == zipWith(List(1, 2, 3), List(4))(_ + _))
    assert(List(5, 7) == zipWith(List(1, 2, 3), List(4, 5))(_ + _))
    assert(List(5, 7, 9) == zipWith(List(1, 2, 3), List(4, 5, 6))(_ + _))
  }

  test("Exercise 3.24") {
    import fp.ch3.Chapter3.hasSubsequence

    assert(hasSubsequence(List(1, 2, 3, 4, 5), List(2, 3)))
    assert(hasSubsequence(List(1, 2, 3, 5, 2, 3, 4), List(2, 3, 4)))
    assert(!hasSubsequence(List(1, 2, 3, 5, 2, 3, 4), List(2, 3, 6)))
    assert(hasSubsequence(List(1, 2, 3, 5, 2, 3, 4), Nil))
    assert(hasSubsequence(List(1, 2, 2, 2, 3, 4), List(2, 2, 3)))
    assert(hasSubsequence(Nil, Nil))
    assert(!hasSubsequence(Nil, List(1)))
  }

  test("Exercise 3.25") {
    import fp.ch3.Chapter3.count

    assert(7 == count(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4)))))
    assert(1 == count(Leaf(1)))
  }

  test("Exercise 3.26") {
    import fp.ch3.Chapter3.maximum

    assert(4 == maximum(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4)))))
    assert(1 == maximum(Branch(Leaf(-1), Leaf(1))))
    assert(1 == maximum(Leaf(1)))
  }

  test("Exercise 3.27") {
    import fp.ch3.Chapter3.depth

    assert(3 == depth(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4)))))
    assert(2 == depth(Branch(Leaf(-1), Leaf(1))))
    assert(1 == depth(Leaf(1)))
  }

  test("Exercise 3.28") {
    import fp.ch3.Chapter3.map

    assert(Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5))) == map(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))(_ + 1))
    assert(Branch(Leaf(0), Leaf(2)) == map(Branch(Leaf(-1), Leaf(1)))(_ + 1))
    assert(Leaf(2) == map(Leaf(1))(_ + 1))
  }

  test("Exercise 3.29") {
    import fp.ch3.Chapter3.{countViaFold, maximumViaFold, depthViaFold, mapViaFold}

    assert(7 == countViaFold(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4)))))
    assert(4 == maximumViaFold(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4)))))
    assert(3 == depthViaFold(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4)))))
    assert(Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(4), Leaf(5))) == mapViaFold(Branch(Branch(Leaf(1), Leaf(2)), Branch(Leaf(3), Leaf(4))))(_ + 1))
  }
}
