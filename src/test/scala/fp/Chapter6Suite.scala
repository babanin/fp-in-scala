package fp

import fp.ch6.{RNG, SimpleRNG}
import org.scalatest.{FunSpec, FunSuite}

/**
  * @author Ivan Babanin (babanin@gmail.com)
  */
class Chapter6Suite extends FunSuite {
  test("Exercise 6.4") {
    import fp.ch6.Chapter6.ints

    val (l, r): (List[Int], RNG) = ints(5)(SimpleRNG(1))
    assert(l.size == 5)
  }

  test("Exercise 6.5") {
    import fp.ch6.Chapter6.doubleMap

    val (d, r) = doubleMap(SimpleRNG(1))
    assert(d <= 1.0)
  }

  test("Exercise 6.7") {
    import fp.ch6.Chapter6.{sequence, sequenceFoldRight, int}

    val (l, rng): (List[Int], RNG) = sequence(List(int, int, int))(SimpleRNG(1))
    val (l2, rng2): (List[Int], RNG) = sequenceFoldRight(List(int, int, int))(SimpleRNG(1))
    assert(l == l2)
  }

}
