package fp

import org.scalatest.FunSuite

/**
  * Created by Ivan_Babanin on 12/20/2016.
  */
class Chapter4Suite extends FunSuite {
  test("Exercise 4.3") {
    import fp.ch4.Chapter4.map2
    import fp.ch4.{Some, None, Option}

    assert(Some(5) == map2(Some(2), Some(3))(_ + _))
    assert(None == map2(Some(2), None: Option[Int])(_ + _))
  }

  test("Exercise 4.4") {
    import fp.ch4._
    import fp.ch4.Chapter4._

    assert(Some(List(1, 2, 3, 4)) == sequence(List(Some(1), Some(2), Some(3), Some(4))))
    assert(None == sequence(List(Some(1), Some(2), None, Some(4))))
    assert(Some(Nil) == sequence(Nil: List[Option[Nothing]]))
  }

  test("Exercise 4.5") {
    import fp.ch4._
    import fp.ch4.Chapter4._

    assert(Some(List(2, 3, 4)) == traverse(List(1, 2, 3))(x => Some(x + 1)))
    assert(None == traverse(List(1, 2, 3))(x => if (x % 2 == 0) None else Some(x)))
  }

  test("Exercise 4.7") {
    import fp.ch4.Chapter4._
    import fp.ch4._

    val as: List[Either[Int, Int]] = List(Right(1), Right(2), Right(3), Left(-1), Right(2))
    assert(Left(-1) == sequenceEither[Int, Int](as))
    assert(Right(List(1, 2)) == sequenceEither(List(Right(1), Right(2))))

    assert(Left(-1) == traverseEither(List(1, 2, 3, -1))(x => if (x < 0) Left(x) else Right(x)))
    assert(Right(List(1, 2, 3)) == traverseEither(List(1, 2, 3))(Right(_)))
  }
}
