package fp

import org.scalatest.FunSuite

class Chapter2Suite extends FunSuite {

  import fp.ch2.Chapter2.{fib, isSorted}

  test("Exercise 2.1") {
    assert(fib(1) == 1)
    assert(fib(2) == 1)
  }

  test("Exercise 2.2") {
    assert(isSorted(Array(1, 2, 3, 4))((a, b) => a < b))
    assert(!isSorted(Array(1, 2, 3, 4))((a, b) => a > b))
  }

  test("Exercise 2.3") {
    import fp.ch2.Chapter2.curry

    val f : (Int, String) => Array[String] = (a, b) => Array(a.toString, b)
    val cf = curry(f)

    assert(cf(1)("2") sameElements Array("1", "2"))
  }

  test("Exercise 2.4") {
    import fp.ch2.Chapter2.uncurry

    val cf : Int => String => Array[String] = (a : Int) => (b : String) => Array(a.toString, b)
    val f = uncurry(cf)

    assert(f(1, "2") sameElements Array("1", "2"))
  }

  test("Exercise 2.5") {
    import fp.ch2.Chapter2.compose

    val ab = (a : Int) => a + 1
    val bc = (b : Int) => b * 2

    assert(compose(bc, ab)(2) == 6)
  }
}
